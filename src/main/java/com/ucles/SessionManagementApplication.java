package com.ucles;

import org.springframework.boot.SpringApplication;

public class SessionManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(SessionManagementApplication.class, args);
    }
}
